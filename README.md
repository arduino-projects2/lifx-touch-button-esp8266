# LIFX touch-button ESP8266
A capacitive-touch sensing light switch toggle, built to work with LIFX bulbs.

Logs WiFi connection phase, calibration and usage to serial monitor for debugging.

## Secrets.h
To setup a connection, ensure you create a `secrets.h` file. See `secrets EXAMPLE.h` for an example.