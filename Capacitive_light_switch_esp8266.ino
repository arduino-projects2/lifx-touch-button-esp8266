#include <CapacitiveSensor.h>

#include "LIFX.h"
#include <ESP8266WiFi.h>

#include "secrets.h"



CapacitiveSensor   cs_4_2 = CapacitiveSensor(5, 22);       // 10 megohm resistor between pins 15 & 19, pin 19 is sensor pin, add wire, foil
int touchDifference = 2000; // Required touch change to trigger button press
int calibrationIterations = 200;
int calibrationIterationsRemaining; // How long to build an average reading on startup
int calibrationTotal;
int idleTouchReading;

void setup() {
//	cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example

	delay(500);

	// Start serial printer
	Serial.begin(74880);
	while (!Serial);
	delay(1000); // Small time to ensure serial is ready
	Serial.println("Starting...");

	WIFISetUp();

	calibrationIterationsRemaining = calibrationIterations;
}

bool buttonDown = false;
void loop() {
	long start = millis();
	long touchReading =  cs_4_2.capacitiveSensor(30);

	if (calibrationIterationsRemaining > 0) {
		calibrationIterationsRemaining--;
		calibrationTotal += touchReading;
	} else if (calibrationIterationsRemaining == 0) {
		calibrationIterationsRemaining = -1;
		idleTouchReading = calibrationTotal / calibrationIterations;
	} else {

		if (abs(idleTouchReading - touchReading) > touchDifference) {
			if (!buttonDown) {
				buttonPressed();
			}
			buttonDown = true;
		} else {
			if (buttonDown) {
				buttonReleased();
			}
			buttonDown = false;
		}
	}
	
	Serial.print("|  Touch: " + (String)touchReading);
	Serial.print("  |  Idle: " + (String)idleTouchReading);
	if (buttonDown) { Serial.println("  |  BUTTON DOWN!  |"); }

	delay(10);                             // arbitrary delay to limit data to serial port
}

int lastPress = 0;
const int buttonCooldown = 100;
void buttonPressed() {
  if (millis() - lastPress < buttonCooldown) {
    return;
  }
  lastPress = millis();
  toggleLight();
}

void buttonReleased() { }

void WIFISetUp(void)
{
	// Connect to Wifi network
	Serial.print("Attempting to connect to Network named: ");
	Serial.println(ssid);           // print the network name (SSID)
	
	// Set WiFi to station mode and disconnect from an AP if it was previously connected
	WiFi.disconnect();
	delay(1000);
	Serial.print("Connecting");
	WiFi.begin(ssid, pass); // Try to connect
	
	byte count = 0;
	while(WiFi.status() != WL_CONNECTED && count < 10)
	{
		count ++;
		delay(1000);
		Serial.print('.');
	}
	Serial.println(""); // Move cursor to new line

	if(WiFi.status() == WL_CONNECTED)
	{
		Serial.println("Connected!");
		delay(500);
	}
	else
	{
		Serial.println("Connection failed!");
		delay(1000);
		WIFISetUp(); // Retry
	}
}
